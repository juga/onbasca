# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

# Import base with `*`, so that django settings
# read all contanstatns
#  F403 'from .base import *' used; unable to detect undefined names
from .base import *  # noqa: F403

DEBUG = True

# F405 'INSTALLED_APPS' may be undefined, or defined from star imports: .base
INSTALLED_APPS.append("django_extensions")  # noqa: F405

# shell_plus
SHELL_PLUS = "ipython"
# print SQL queries in shell_plus
SHELL_PLUS_PRINT_SQL = True
