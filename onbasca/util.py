# SPDX-FileCopyrightText: 2023 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause
import datetime
import logging

from onbasca.onbasca import config

logger = logging.getLogger(__name__)


def delete_old(obj, sender, days=config.OLDEST_DATA_DAYS):
    old = datetime.datetime.utcnow() - datetime.timedelta(days=days)
    for obj in sender.objects.filter(_obj_updated_at__lt=old):
        logger.info("Deleting old %s: %s", sender, obj)
        obj.delete()
