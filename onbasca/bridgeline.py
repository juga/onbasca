# SPDX-FileCopyrightText: 2023 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause
"""
Validate bridgeline so that it can be set to tor configuration as ``Bridge``.

If a bridgeline is malformed, stem's ``set_conf`` would fail for all the
bridgelines.

Bridge [transport] IP:PORT [id-fingerprint] [k=v] [k=v]

"""
import ipaddress
import logging
import re
from base64 import b16decode
from urllib import parse

from onbrisca import defaults

# logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

DIGEST_LEN = 20
HEX_DIGEST_LEN = 40
MAX_SOCKS5_AUTH_FIELD_SIZE = 255
MAX_SOCKS5_AUTH_SIZE_TOTAL = 2 * MAX_SOCKS5_AUTH_FIELD_SIZE


def validate_transport_socks_arguments(socks_args):
    """
    Validate transport socks arguments.

    Function based on
    https://gitlab.torproject.org/tpo/core/tor/-/blob/3d63d713ea42d1ed1ca4686340cd03f82ba394b7/src/app/config/config.c#L5056

    """
    # logger.debug("socks args %s", socks_args)
    expr = re.compile(defaults.REGEX_PT_SOCKS_ARGS)
    for s in socks_args:
        if not expr.match(s):
            logger.warning("'%s' is not a k=v item.", s)
            return None
    socks_string = " ".join(socks_args)
    socks_string_len = len(socks_string)
    if socks_string_len > MAX_SOCKS5_AUTH_SIZE_TOTAL:
        logger.warning(
            "SOCKS arguments can't be more than %u bytes (%lu).",
            MAX_SOCKS5_AUTH_SIZE_TOTAL,
            socks_string_len,
        )
        return None
    return socks_string


def addrport_parse(addrport):
    """
    Parse a socket address in the form "host[:port].

    This function is similar to
    https://gitlab.torproject.org/tpo/core/tor/-/blob/3d63d713ea42d1ed1ca4686340cd03f82ba394b7/src/lib/net/address.c#L1839,  # noqa
    but it doesn't check any other IP ranges other than the ones that ``ipaddress``
    checks.

    """
    port = 443
    try:
        ip = ipaddress.ip_address(addrport)
    except ValueError:
        try:
            parsed = parse.urlparse("//{}".format(addrport))
        except Exception:
            return None
        try:
            ip = ipaddress.ip_address(parsed.hostname)
        except Exception:
            return None
        port = parsed.port or port
    except Exception:
        return None
    if isinstance(ip, ipaddress.IPv6Address):
        return "[{}]:{}".format(ip, port)
    return "{}:{}".format(ip, port)


def parse_bridge_line(bridgeline):
    """
    Parse the contents of a string containing a Bridge line

    Function based on
    https://gitlab.torproject.org/tpo/core/tor/-/blob/3d63d713ea42d1ed1ca4686340cd03f82ba394b7/src/app/config/config.c#L5107  # noqa

    Validates that the IP:PORT, fingerprint, and SOCKS arguments (given to the
    Pluggable Transport, if a one was specified) are well-formed.

    Returns None If the Bridge line could not be validated, and returns a
    dictionary containing the parsed information otherwise.

    The dictionary keys are sets as they're validated.

    Bridge line format:
    Bridge [transport] IP:PORT [id-fingerprint] [k=v] [k=v] ...

    """
    bridge_dict = {}
    transport_name = addrport = fingerprint = socks_string = None
    socks_args = []
    items = bridgeline.split(" ")
    if len(items) < 1:
        logger.warning("Too few arguments to Bridge line.")
        return None
    # first field is either a transport name or addrport
    field = items[0]
    items = items[1:]
    # Instead of parsing the name of transport, just check whether it's in
    # `PT_TRANSPORTS`, as currently the scanner doesn't know how to check other
    # transports.
    if field in defaults.PT_TRANSPORTS:
        # It's a transport name.
        transport_name = field
        if len(items) < 1:
            logger.warning("Too few items to Bridge line.")
            return None
        bridge_dict["transport_name"] = transport_name
        # Next field is addrport then.
        addrport = items[0]
        items = items[1:]
    else:
        addrport = field
    addrport = addrport_parse(addrport)
    if not addrport:
        logger.warning(
            "Error parsing Bridge address '%s' for bridgeline %s.",
            addrport,
            bridgeline,
        )
        return None
    bridge_dict["addrport"] = addrport
    # If transports are enabled, next field could be a fingerprint or a
    # socks argument. If transports are disabled, next field must be
    # a fingerprint
    if items:
        if transport_name:
            field = items[0]
            items = items[1:]
            # If it's a key=value pair, then it's a SOCKS argument for the
            # transport proxy..
            expr = re.compile(defaults.REGEX_PT_SOCKS_ARGS)
            if expr.match(field):
                socks_args = [field]
            else:
                fingerprint = field
        else:
            fingerprint = items[0]
    # Handle fingerprint, if it was provided.
    if fingerprint:
        if len(fingerprint) != HEX_DIGEST_LEN:
            logger.warning("Key digest for Bridge is wrong length.")
            return None
        if len(b16decode(fingerprint.upper())) != DIGEST_LEN:
            logger.warning("Unable to decode Bridge key digest.")
            return None
        bridge_dict["fingerprint"] = fingerprint
    # If we are using transports, any remaining items should be k=v values
    if transport_name and items:
        # append remaining items of 'items' to 'socks_args'
        socks_args.extend(items)
        items = []
    if socks_args:
        socks_string = validate_transport_socks_arguments(socks_args)
        if not socks_string:
            return None
        bridge_dict["socks_args"] = socks_args
    # logger.debug("Successfully parsed bridgeline %s", bridge_dict)
    return (addrport, fingerprint, transport_name, socks_string)


def validate_bridgelines(bridgelines):
    """Return valid and invalid bridgelines"""
    valid_bridgelines = invalid_bridgelines = []
    for bridgeline in bridgelines:
        if parse_bridge_line(bridgeline):
            valid_bridgelines.append(bridgeline)
        else:
            invalid_bridgelines.append(bridgeline)
    return valid_bridgelines, invalid_bridgelines
