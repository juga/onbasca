# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

import datetime
import logging

from django.db import models

from . import BaseModel

logger = logging.getLogger(__name__)


class ConsensusManagerBase(models.Manager):
    def from_router_statuses(self, router_statuses, valid_after=None):
        if not valid_after:
            router_status = router_statuses[0]
            try:
                valid_after = router_status.document.valid_after
            except Exception:
                pass
        if not valid_after:
            valid_after = datetime.datetime.utcnow().replace(microsecond=0)
        consensus, created = self.get_or_create(valid_after=valid_after)
        logger.info("Consensus %s created: %s", consensus, created)
        return consensus


class ConsensusBase(BaseModel):
    class Meta:
        abstract = True

    objects = ConsensusManagerBase()
    valid_after = models.DateTimeField(primary_key=True, editable=True)

    def __str__(self):
        return "{}".format(self.valid_after)

    def relays_count(self):
        return self.relay_set.count()

    def relays(self):
        self.relay_set.all()

    def routerstatuses(self):
        return self.routerstatus_set.all()

    # for admin
    def routerstatuses_count(self):
        return self.routerstatus_set.count()
