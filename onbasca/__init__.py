# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

from ._version import get_versions

__version__ = get_versions()["version"]
del get_versions
