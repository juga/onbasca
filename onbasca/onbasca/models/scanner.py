# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

import concurrent.futures
import datetime
import logging
import signal
import sys
import traceback
import uuid

from django.db import models

from onbasca.onbasca import config
from onbasca.onbasca.models.heartbeat import Heartbeat
from onbasca.onbasca.models.webserver import WebServer
from onbasca.onbasca.torcontrol import TorControl

logger = logging.getLogger(__name__)


class Scanner(models.Model):
    """Singleton class that manages the measurer threads.

    And initializes all the needed objects.

    https://gitlab.torproject.org/tpo/network-health/sbws/-/issues/40092,
    https://gitlab.torproject.org/tpo/network-health/sbws/-/issues/40084,
    https://gitlab.torproject.org/tpo/network-health/sbws/-/issues/40087:
    solved by using concurrent.futures.

    """

    _obj_created_at = models.DateTimeField(auto_now_add=True, null=True)
    _obj_updated_at = models.DateTimeField(auto_now=True, null=True)

    started_at = models.DateTimeField(default=datetime.datetime.utcnow)
    elapsed_time = models.DurationField(null=True, blank=True)

    # More static metadata
    uuid = models.UUIDField(default=uuid.uuid4, unique=True)
    nickname = models.CharField(max_length=64, default=config.NICKNAME)
    tor_version = models.CharField(max_length=64, null=True, blank=True)

    heartbeat = models.OneToOneField(
        "Heartbeat", null=True, blank=True, on_delete=models.CASCADE
    )

    def init(self, port=None, socket=None):  # , *args, **kwargs):
        heartbeat = Heartbeat()
        heartbeat.save()
        self.heartbeat = heartbeat
        logger.info("Heartbeat %s created.", self.heartbeat)
        WebServer.objects.update_from_dict(config.WEB_SERVERS)
        self.tor_control = TorControl()
        self.tor_control.launch_or_connect_tor(port=port, socket=socket)

        self.socks_address = self.tor_control.get_socks_address()
        self.tor_version = str(self.tor_control.controller.get_version())
        self.save()
        logger.info("Tor version %s.", self.tor_version)
        self.session_kwargs = {
            "nickname": self.nickname,
            "uuid": self.uuid,
            "tor_version": self.tor_version,
        }
        logger.debug(self.session_kwargs)
        logger.info("Initialized.")
        self.future_measurements = self.executor = None

        # Only the 1st time
        self.consensus = self.tor_control.obtain_relays()

    def save(self, *args, **kwargs):
        logger.info("Saving Scanner.")
        self.pk = 1
        super(Scanner, self).save(*args, **kwargs)

    @classmethod
    def load(cls):
        logger.info("Loading Scanner.")
        obj, created = cls.objects.get_or_create(pk=1)
        return obj

    def __str__(self):
        return "{}-{}".format(self.started_at, self.uuid)

    def set_elapsed_time(self):
        if self.started_at:
            self.elapsed_time = datetime.datetime.utcnow() - self.started_at
        else:
            self.elapsed_time = 0
        self.save()
        return self.elapsed_time

    def queue_future_measurements(self):
        """

        https://gitlab.torproject.org/tpo/network-health/sbws/-/issues/40037:
        solved by measuring only the relays in the consensus.

        """
        logger.info("Queuing future measurements")
        with concurrent.futures.ThreadPoolExecutor(
            max_workers=config.NUM_THREADS, thread_name_prefix="measurer"
        ) as self.executor:
            logger.info("In the executor, queue all future measurements.")
            self.future_measurements = {
                self.executor.submit(
                    self.tor_control.measure_relay,
                    routerstatus.relay,
                    self.session_kwargs,
                    self.socks_address,
                ): routerstatus
                for routerstatus in self.consensus.routerstatuses_ordered()
            }
        self.save()
        logger.info(
            "Finished with %s queued futures."
            " Measurements might not be ready yet.",
            len(self.future_measurements),
        )
        return self.future_measurements

    def obtain_future_measurements(self):
        logger.info("Obtain measurements as they finish.")
        counter = 0
        for future_measurement in concurrent.futures.as_completed(
            self.future_measurements
        ):
            router_status = self.future_measurements[future_measurement]
            logger.debug(
                "Future measurement for router status %s is done: %s",
                router_status,
                future_measurement.done(),
            )
            try:
                measurement = future_measurement.result()
            except Exception as e:
                logger.debug(
                    "%s generated an exception: %s, %s"
                    % (router_status, e, type(e))
                )
                traceback.print_exc()
                counter += 1
            else:
                logger.info("Measurement ready: %s" % (measurement))
                counter += 1
        logger.info("Finished with %s measurements.", counter)

    def wait_last_future_measurements(self):
        logger.info("Wait for any remaining measurements.")
        concurrent.futures.wait(
            self.future_measurements,
            timeout=config.FUTURE_TIMEOUT_SECS,
            return_when=concurrent.futures.ALL_COMPLETED,
        )
        logger.info("Finished with all measurements.")

    def stop_threads(self, signal, frame, exit_code=0):
        logger.info(
            "Stopping threads with signal %s, frame %s and code %s.",
            signal,
            frame,
            exit_code,
        )
        logger.debug("Executor threads: %s", self.executor._threads)
        if self.future_measurements:
            for (
                future_measurement,
                router_status,
            ) in self.future_measurements.items():
                cancelled = future_measurement.cancel()
                logger.debug(
                    "Router status %s with future: %s is cancelled: %s",
                    router_status,
                    future_measurement,
                    cancelled,
                )
        if exit_code:
            logger.error("Threads have been stop. Exiting.")
        sys.exit(exit_code)

    def scan(self):
        while True:
            if (
                self.tor_control.controller.get_conf("TestingTorNetwork")
                == "1"
                and self.heartbeat.loops_count >= 3
            ):
                logger.debug("Testing network and 3 loops, exiting.")
                self.stop_threads(signal.SIGTERM, None, 0)
                return
            self.future_measurements = self.queue_future_measurements()
            self.obtain_future_measurements()
            self.wait_last_future_measurements()
            self.heartbeat.increment_loops()
            self.heartbeat.log_status()
            logger.info("Finished a loop.")

    def run(self):
        logger.info("Starting scanner.")
        try:
            self.scan()
        except KeyboardInterrupt:
            logger.info("Interrupted by the user.")
            self.stop_threads(signal.SIGINT, None)
        except Exception as e:
            logger.exception(e)
            self.stop_threads(signal.SIGTERM, None, 1)
