# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

import datetime
import logging

from django.db import IntegrityError, models

from onbasca.base.models.relaybw import RelayBwBase, RelayBwManagerBase
from onbasca.onbasca import config, constants

from .. import relaybw_validators
from .relay import Relay

logger = logging.getLogger(__name__)


BWLINE_KEYS = [
    "node_id",
    "nick",
    "bw",
    "bw_mean",
    "bw_median",
    "consensus_bandwidth",
    "consensus_bandwidth_is_unmeasured",
    "desc_bw_avg",
    "desc_bw_bur",
    "desc_bw_obs_last",
    "desc_bw_obs_mean",
    "error_circ",
    "error_destination",
    "error_misc",
    "error_second_relay",
    "error_stream",
    "master_key_ed25519",
    "relay_in_recent_consensus_count",
    "relay_recent_measurement_attempt_count",
    "relay_recent_measurements_excluded_error_count",
    "relay_recent_priority_list_count",
    "success",
    "time",
    "measured_at",
    "updated_at",
    "unmeasured",
    "vote",
    "under_min_report",
    "circ_fail",
    "pid_delta",
    "pid_bw",
    "pid_error",
    "pid_error_sum",
]
# When `vote=0` these are the only KeyValues that appear
BWLINE_KEYS_VOTE_0 = [
    "node_id",
    "nick",
    "bw",
    "error_circ",
    "error_destination",
    "error_misc",
    "error_second_relay",
    "error_stream",
    "master_key_ed25519",
    "relay_in_recent_consensus_count",
    "relay_recent_measurement_attempt_count",
    "relay_recent_measurements_excluded_error_count",
    "relay_recent_priority_list_count",
    "success",
    "time",
    "measured_at",
    "updated_at",
    "unmeasured",
    "vote",
    "under_min_report",
    "circ_fail",
    "pid_delta",
    "pid_bw",
    "pid_error",
    "pid_error_sum",
]
BWLINE_KEYS_FROM_RELAY = [
    "nickname",
    "consensus_bandwidth",
    "consensus_bandwidth_is_unmeasured",
    "desc_bw_avg",
    "desc_bw_bur",
    "desc_bw_obs_last",
    "desc_bw_obs_mean",
    "master_key_ed25519",
    "error_circ",
    "error_stream",
    "error_misc",
    "error_destination",  # This is not being used atm.
]
BWLINE2RELABW = {
    "node_id": "fingerprint",
    "nick": "nickname",
    # "time": "measured_at",
    # "measured_at": "updated_at"
}


class RelayBwManager(RelayBwManagerBase):
    def set_attrs(self, relaybw, relay):
        for attr in BWLINE_KEYS_FROM_RELAY:
            setattr(relaybw, attr, getattr(relay, attr, None))
        relaybw.save()
        return relaybw

    def from_relay(self, relay, now=None):
        """

        https://gitlab.torproject.org/tpo/network-health/sbws/-/issues/34444,
        https://gitlab.torproject.org/tpo/network-health/sbws/-/issues/34309,
        others: `relay_recent` KeyValues are updated correctly since their
        values come from the DB and not queues of timestamps.

        """
        now = now or datetime.datetime.utcnow().replace(microsecond=0)
        days_ago = now - datetime.timedelta(days=config.RECENT_DAYS)
        relaybw = RelayBw(fingerprint=relay.fingerprint)
        relaybw = self.set_attrs(relaybw, relay)
        # Querysets with conensuses
        relaybw.relay_in_recent_consensus_count = relay.consensuses.filter(
            valid_after__gt=days_ago
        ).count()
        recent_measurements = relay.measurements.filter(
            created_at__gte=days_ago
        )
        # Querysets related with measurements
        if recent_measurements.count() > 0:
            # success is int, not boolean
            relaybw.success = recent_measurements.filter(
                bandwidth__isnull=False
            ).count()
            relaybw.time = (
                relaybw.measured_at
            ) = relay.measurement_latest_date()
            relaybw.relay_recent_measurements_excluded_error_count = (
                recent_measurements.filter(error__isnull=False).count()
            )
            relaybw.relay_recent_priority_list_count = (
                recent_measurements.filter(queued_at__isnull=False).count()
            )
            relaybw.relay_recent_measurement_attempt_count = (
                recent_measurements.filter(attempted_at__isnull=False).count()
            )
            relaybw.relay_recent_measurement_failure_count = (
                relaybw.relay_recent_priority_list_count
                - recent_measurements.filter(finished_at__isnull=False).count()
            )
        relaybw.relay = relay
        # `full_clean` will call `clean_fields`, which call the field's
        # `validators`` and then `clean`.
        # Not adding `full_clean` in `save``, since there're other calls to
        # save in which the values are not calculated yet.
        relaybw.full_clean()
        try:
            relaybw.save()
        except IntegrityError as e:
            # This happen because the old Relays get deleted when saving
            # the Relay bw_mean of the RelayBw?
            logger.warning(e)
        return relaybw


class RelayBw(RelayBwBase):
    objects = RelayBwManager()

    bwfile = models.ForeignKey(
        "BwFile", on_delete=models.CASCADE, null=True, blank=True
    )
    relay = models.ForeignKey(
        Relay, on_delete=models.CASCADE, null=True, blank=True
    )
    bw_mean = models.PositiveIntegerField(null=True, blank=True)
    bw_median = models.PositiveIntegerField(null=True, blank=True)
    desc_bw_obs_mean = models.PositiveIntegerField(null=True, blank=True)
    error_destination = models.PositiveSmallIntegerField(null=True, blank=True)
    error_misc = models.PositiveSmallIntegerField(null=True, blank=True)
    error_second_relay = models.PositiveSmallIntegerField(
        null=True, blank=True
    )
    master_key_ed25519 = models.CharField(max_length=64, null=True, blank=True)
    time = models.DateTimeField(null=True, blank=True)
    unmeasured = models.BooleanField(null=True, blank=True)

    scanner = models.CharField(max_length=255, null=True, blank=True)

    # KeyValues defined in `base` repeated here to add validators.
    relay_in_recent_consensus_count = models.PositiveSmallIntegerField(
        null=True,
        blank=True,
        default=0,
        validators=[
            relaybw_validators.validate_relay_in_recent_consensus_count_max
        ],
    )
    relay_recent_priority_list_count = models.PositiveSmallIntegerField(
        null=True,
        blank=True,
        default=0,
        validators=[
            relaybw_validators.validate_relay_recent_priority_list_count_max
        ],
    )
    relay_recent_measurement_attempt_count = models.PositiveSmallIntegerField(
        null=True,
        blank=True,
        default=0,
        validators=[
            relaybw_validators.validate_relay_recent_measurement_attempt_count_max  # noqa:E501
        ],
    )
    relay_recent_measurement_failure_count = models.PositiveSmallIntegerField(
        null=True,
        blank=True,
        default=0,
        validators=[
            relaybw_validators.validate_relay_recent_measurement_failure_count_max  # noqa:E501
        ],
    )
    relay_recent_measurements_excluded_error_count = models.PositiveSmallIntegerField(  # noqa:E501
        null=True,
        blank=True,
        default=0,
        validators=[
            relaybw_validators.validate_relay_recent_measurements_excluded_error_count_max  # noqa:E501
        ],
    )

    # Useful for calculations
    _bw_filt = models.PositiveIntegerField(null=True, blank=True)
    _ratio_stream = models.FloatField(null=True, blank=True)
    _ratio_filt = models.FloatField(null=True, blank=True)
    _ratio = models.FloatField(null=True, blank=True)

    _bw_scaled = models.FloatField(null=True, blank=True)
    _bw_scaled_limited = models.FloatField(null=True, blank=True)
    _bw_scaled_limited_rounded = models.PositiveIntegerField(
        null=True, blank=True
    )

    def __str__(self):
        return self.nickname

    # To generate relaybw
    def set_bw_scaled(self):
        if not self.relay:
            logger.warning(
                "RelayBw %s without Relay, can not scale bandwidth. "
            )
            return None
        self.bw_mean = self.relay.set_bw_mean()
        self._bw_filt = self.relay.set_bw_filt()
        if self.bw_mean > self._bw_filt:
            logger.debug("Relay bw stream greater than filtered.")
        self.bw_median = self.relay.set_bw_median()  # Only for the bwfile
        self._ratio_stream = self.relay.set_ratio_stream(self.bwfile._mu)
        self._ratio_filt = self.relay.set_ratio_filt(self.bwfile._muf)
        self._ratio = self.relay.set_ratio()
        self._bw_scaled = self.relay.set_bw_scaled()
        self.save()
        return self._bw_scaled

    def set_bw_scaled_limited(self):
        self._bw_scaled_limited = min(
            self._bw_scaled, self._bw_scaled * self.bwfile._limit
        )
        logger.debug("Relay limited bw: %s", self._bw_scaled_limited)
        self.save()
        return self._bw_scaled_limited

    def set_bw_scaled_limited_rounded(self):
        self.bw = self._bw_scaled_limited_rounded = (
            round(self._bw_scaled_limited / constants.KB * 1.0) or 1
        )
        logger.debug("Relay rounded bw: %s", self._bw_scaled_limited_rounded)
        self.save()
        return self._bw_scaled_limited_rounded

    def set_vote(self):
        """Set ``vote`` and ``unmeasured`` KeyValues.

        From [BandwidthFile]_ ::

          generator implementations
          MUST set "bw=1" for unmeasured relays. Using the minimum bw value
          makes authorities that do not understand "vote=0" or "unmeasured=1"
          produce votes that don't change relay weights too much.

        """
        if not self.success:
            self.vote = False
            self.unmeasured = True
            self.bw = 1
            self.save()

    def set_under_min_report(self):
        """Set ``vote`` and ``unmeasured`` KeyValues.

        From [BandwidthFile]_ ::

          generator implementations
          MUST NOT change the bandwidths for under_min_report relays. Using the
          same bw value makes authorities that do not understand "vote=0"
          or "under_min_report=1" produce votes that don't change relay weights
          too much. It also avoids flapping when the reporting threshold is
          reached.

        """
        self.under_min_report = True
        self.vote = False
        self.save()

    def to_str_v15(self):
        from onbasca.base import constants

        # When `vote=0` not all KeyValues appear
        # Note: Probably better to use the same KeyValues in version 3.
        if self.vote == 0:
            kwargs = dict(
                [(k, getattr(self, k, "")) for k in BWLINE_KEYS_VOTE_0]
            )
        else:
            kwargs = dict([(k, getattr(self, k, "")) for k in BWLINE_KEYS])

        # Change fingerprint by node_id and nickname by nick
        for k, v in BWLINE2RELABW.items():
            kwargs[k] = getattr(self, v, None)
        kwargs["node_id"] = "$" + kwargs["node_id"]

        # Remove measured_at and update_at
        kwargs.pop("measured_at")
        kwargs.pop("updated_at")

        # When vote is True or None
        if kwargs["vote"] is not False:
            kwargs.pop("vote")
        # Remove KeyValues that do not appear when they're False
        if not kwargs["unmeasured"]:
            kwargs.pop("unmeasured")
        if not kwargs["under_min_report"]:
            kwargs.pop("under_min_report")

        key_values = []
        for key, value in sorted(kwargs.items()):
            # logger.debug("RelayBw key: %s, value: %s", key, value)
            # Ignore Torflow KeyValues atm.
            if key.startswith("pid") or key.startswith("circ"):
                continue
            # Convert boolean values to int
            if isinstance(value, bool):
                value = int(value)
            # Convert null values to 0
            if value is None:
                value = 0
            # Format datetime values
            if isinstance(value, datetime.datetime):
                value = value.strftime(constants.DATETIME_FORMAT)

            key_values.append("{}={}".format(key, value))
        key_values_str = " ".join(key_values)
        return key_values_str

    def to_str(self):
        return self.to_str_v15()

    def clean(self):
        # logger.debug("Validating recent KeyValues.")
        d = dict(
            [
                (k, v)
                for k, v in self.__dict__.items()
                if k.startswith("relay_")
            ],
        )
        relaybw_validators.validate_recent_keyvalues(d)
