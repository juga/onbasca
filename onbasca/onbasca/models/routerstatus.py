# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

import logging
import random

from django.db import models
from stem import Flag

from onbasca.base.models.routerstatus import (
    RouterStatusBase,
    RouterStatusManagerBase,
)

from .relay import Relay

logger = logging.getLogger(__name__)


class RouterStatusManager(RouterStatusManagerBase):
    def from_router_status(self, router_status, consensus=None):
        kwargs = {
            "_exit": Flag.EXIT in router_status.flags
            and Flag.GUARD not in router_status.flags,
            "_guard": Flag.GUARD in router_status.flags
            and Flag.EXIT not in router_status.flags,
            "_guard_exit": Flag.GUARD in router_status.flags
            and Flag.EXIT in router_status.flags,
            "_middle": Flag.GUARD not in router_status.flags
            and Flag.EXIT not in router_status.flags,
            "_authority": Flag.AUTHORITY in router_status.flags,
            "_running": Flag.RUNNING in router_status.flags,
            "_valid": Flag.VALID in router_status.flags,
            "_fast": Flag.FAST in router_status.flags,
            "_stable": Flag.STABLE in router_status.flags,
        }
        relay, _ = Relay.objects.get_or_create(
            fingerprint=router_status.fingerprint
        )
        relay.consensuses.add(consensus)
        kwargs["relay"] = relay
        rs = super().from_router_status(router_status, consensus, **kwargs)
        return rs

    def mean_bandwidth(self):
        return self.aggregate(models.Avg("consensus_bandwidth"))[
            "consensus_bandwidth_avg"
        ]


class RouterStatus(RouterStatusBase):

    """

    Torflow::

        # Routers can fall out of our consensus five different ways:
        # 1. Their descriptors disappear
        # 2. Their NS documents disappear
        # 3. They lose the Running flag
        # 4. They list a bandwidth of 0
        # 5. They have 'opt hibernating' set
        routers = self.c.read_routers(nslist) # Sets .down if 3,4,5

        # down = dead
        removed_idhexes.update(set(map(lambda r: r.idhex,
                                   filter(lambda r: r.down, routers))))

        for i in removed_idhexes:
          if i not in self.routers: continue
          self.routers[i].down = True
          if "Running" in self.routers[i].flags:
            self.routers[i].flags.remove("Running")
          if self.routers[i].refcount == 0:
            self.routers[i].deleted = True

          else:
            self.routers[i].deleted = True

    """

    objects = RouterStatusManager()
    relay = models.ForeignKey(
        "Relay", on_delete=models.CASCADE, null=True, blank=True
    )
    consensus = models.ForeignKey(
        "Consensus", on_delete=models.CASCADE, null=True, blank=True
    )
    _exit = models.BooleanField(null=True, blank=True)
    _guard = models.BooleanField(null=True, blank=True)
    _guard_exit = models.BooleanField(null=True, blank=True)
    _middle = models.BooleanField(null=True, blank=True)
    _authority = models.BooleanField(null=True, blank=True)
    _running = models.BooleanField(null=True, blank=True)
    _valid = models.BooleanField(null=True, blank=True)
    _fast = models.BooleanField(null=True, blank=True)
    _stable = models.BooleanField(null=True, blank=True)

    _measurement_latest = models.ForeignKey(
        "Measurement", on_delete=models.SET_NULL, null=True, blank=True
    )

    def is_exit_can_exit_443(self):
        return self.relay.is_exit_can_exit_443()

    def has_2_in_flowctrl(self):
        return self.relay.has_2_in_flowctrl()

    def use_as_exit(self):
        """Return whether or not to use the relay as exit."""
        if not self.is_exit_can_exit_443():
            logger.debug("Relay is not an exit, use it as entry.")
            return False
        if self.consensus._cc_alg_2:
            logger.debug("Congestion control supported.")
            if self.consensus._bwscanner_cc_gte_1:
                logger.debug("Congestion control enabled.")
                if self.has_2_in_flowctrl():
                    logger.debug("Relay has 2 in FlowCtrl.")
                    logger.debug("Use it as exit.")
                    return True
                logger.debug("Relay has NOT 2 in FlowCtrl.")
                logger.debug("Use it as entry.")
                return False
            # _bwscanner_cc < 1
            logger.debug("Congestion control disabled.")
            if self.has_2_in_flowctrl():
                logger.debug("Relay has 2 in FlowCtrl.")
                logger.debug("Use it as entry.")
                return False
            logger.debug("Relay has NOT 2 in FlowCtrl.")
            logger.debug("Use it as exit.")
            return True
        return True

    def helper_candidates(self, relay_as_exit):
        if relay_as_exit:
            candidates_fingerprints = (
                self.consensus.non_exits_with_bandwidth_fingerprints(self)
            )
            logger.debug("Select non exits.")
            return candidates_fingerprints
        if self.consensus._cc_alg_2:
            logger.debug("Congestion control supported.")
            if self.consensus._bwscanner_cc_gte_1:
                logger.debug("Congestion control enabled.")
                logger.debug("Select exits with 2 in FlowCtrl")
                candidates_fingerprints = (
                    self.consensus.exits_with_2_in_flowctrl_fingerprints()
                )
                return candidates_fingerprints
            # _bwscanner_cc < 1
            logger.debug("Congestion control disabled.")
            logger.debug("Select exits WITHOUT 2 in FlowCtrl.")
            candidates_fingerprints = (
                self.consensus.exits_without_2_in_flowctrl_fingerprints()
            )
            return candidates_fingerprints
        logger.debug("Select exits.")
        candidates_fingerprints = (
            self.consensus.exits_with_bandwidth_fingerprints(self)
        )
        return candidates_fingerprints

    def helper_path(self):
        """

        https://gitlab.torproject.org/tpo/network-health/sbws/-/issues/40041:
        do not measure non-exit as exits.

        """
        relay_as_exit = self.use_as_exit()
        candidates_fingerprints = self.helper_candidates(relay_as_exit)
        if not candidates_fingerprints:
            logger.warning("No helper candidates")
            return []
        helper_fp = random.choice(candidates_fingerprints)
        if relay_as_exit:
            path = [helper_fp, self.fingerprint]
            logger.debug("Measuring %s as exit with path: %s", self, path)
            return path
        path = [self.fingerprint, helper_fp]
        logger.debug("Measuring %s as entry with path: %s", self, path)
        return path

    def set_measurement_latest(self, measurement):
        self._measurement_latest = measurement
        self.save()
        return self._measurement_latest
