# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

"""BwFile validators.

Validators to ensure the fields of a BwFile and RelayBw have values within the
correct range.

"""
import datetime
import logging

from onbasca.onbasca import config, util

logger = logging.getLogger(__name__)


# The following `validate_recent_*_max` functions are not in just one function
# cause they are used as validators for the `BwFile` fields.
# See https://docs.djangoproject.com/en/4.0/ref/validators/
def validate_max_value_log(key, value, maximum):
    logger.warning(
        "%s (%s) is greater than the maximum (%s).",
        key,
        value,
        maximum,
    )


def validate_recent_consensus_count_max(value):
    key = "recent_consensus_count"
    maximum = config.MAX_RECENT_CONSENSUS_COUNT
    if value > maximum:
        validate_max_value_log(key, value, maximum)


def validate_recent_priority_list_count_max(value):
    key = "recent_priority_list_count"
    maximum = config.MAX_RECENT_PRIORITY_LIST_COUNT
    if value > maximum:
        validate_max_value_log(key, value, maximum)


def validate_recent_priority_relay_count_max(value):
    key = "recent_priority_relay_count"
    maximum = config.MAX_RECENT_PRIORITY_RELAY_COUNT
    if value > maximum:
        validate_max_value_log(key, value, maximum)


def validate_recent_measurement_attempt_count_max(value):
    key = "recent_measurement_attempt_count"
    maximum = config.MAX_RECENT_PRIORITY_RELAY_COUNT
    if value > maximum:
        validate_max_value_log(key, value, maximum)


def validate_recent_measurement_failure_count_max(value):
    key = "recent_measurement_failure_count"
    maximum = config.MAX_RECENT_PRIORITY_RELAY_COUNT
    if value > maximum:
        validate_max_value_log(key, value, maximum)


def validate_recent_measurements_excluded_error_count_max(value):
    key = "recent_measurements_excluded_error_count"
    maximum = config.MAX_RECENT_PRIORITY_RELAY_COUNT
    if value > maximum:
        validate_max_value_log(key, value, maximum)


def validate_recent_measurements_excluded_near_count_max(value):
    key = "recent_measurements_excluded_near_count"
    if value > 0:
        validate_max_value_log(key, value, 0)


def validate_recent_measurements_excluded_few_count_max(value):
    key = "recent_measurements_excluded_few_count"
    if value > 0:
        validate_max_value_log(key, value, 0)


def validate_recent_measurements_excluded_old_count_max(value):
    key = "recent_measurements_excluded_old_count"
    if value > 0:
        validate_max_value_log(key, value, 0)


def validate_min_value_log(key, value, minimum):
    logger.warning(
        "%s (%s) is less than the minimum (%s).",
        key,
        value,
        minimum,
    )


def validate_recent_keyvalues_min(d, now=None):
    """Validate `recent` KeyValues minima.

    To calculate the minimum Values, it's needed to load the `Scanner` object
    to know when it started, but it can't be done in the moment the migrations
    are created, as the model/object does not exists yet.
    Therefore, calculate them when saving the objects.

    """

    from .models.scanner import Scanner

    now = now or datetime.datetime.utcnow()
    scanner = Scanner.load()
    scanner_duration = now - scanner.started_at
    logger.info("Scanner duration: %s", scanner_duration)
    # The following `MIN_` constants can't be in defaults.py cause they are
    # only known at runtime.
    # In case the scanner started more than `RECENT_DAYS`, just take the
    # recent days
    MIN_RECENT_CONSENSUS_COUNT = (
        min(scanner_duration.days, config.RECENT_DAYS) * 24
    )
    MIN_RECENT_PRIORITY_LIST_COUNT = MIN_RECENT_CONSENSUS_COUNT
    MIN_RECENT_PRIORITY_RELAY_COUNT = (
        MIN_RECENT_PRIORITY_LIST_COUNT * config.MIN_RELAYS_PER_PRIORITY_LIST
    )
    # The following are all the same as MIN_RECENT_PRIORITY_RELAY_COUNT.
    # Leaving them commented for clarity.
    # MIN_RECENT_MEASUREMENT_ATTEMPT_COUNT = MIN_RECENT_PRIORITY_RELAY_COUNT
    # MIN_RECENT_MEASUREMENT_FAILURE_COUNT = MIN_RECENT_MEASUREMENT_ATTEMPT_COUNT  # noqa:E501
    # MIN_RECENT_MEASUREMENTS_EXCLUDED_ERROR_COUNT = (
    #     MIN_RECENT_MEASUREMENT_ATTEMPT_COUNT
    # )
    # MIN_RECENT_MEASUREMENT_FAILURE_COUNT and
    # MIN_RECENT_MEASUREMENTS_EXCLUDED_ERROR_COUNT are not validated cause
    # ideally they're 0 but we know it is normal that there will be some
    # errors.

    logger.debug("Validating recent KeyValues minimum.")

    key = "recent_consensus_count"
    value = d.get(key, None)
    minimum = MIN_RECENT_CONSENSUS_COUNT
    if value < minimum:
        validate_min_value_log(key, value, minimum)

    key = "recent_priority_list_count"
    value = d.get(key, None)
    minimum = MIN_RECENT_PRIORITY_LIST_COUNT
    if value < minimum:
        validate_min_value_log(key, value, minimum)

    key = "recent_priority_relay_count"
    value = d.get(key, None)
    minimum = MIN_RECENT_PRIORITY_RELAY_COUNT
    if value < minimum:
        validate_min_value_log(key, value, minimum)

    key = "recent_measurement_attempt_count"
    value = d.get(key, None)
    minimum = MIN_RECENT_PRIORITY_RELAY_COUNT
    if value < minimum:
        validate_min_value_log(key, value, minimum)


def validate_recent_keyvalues(d):
    logger.debug("Validating recent KeyValues.")

    key1 = "recent_consensus_count"
    value1 = d.get(key1, 0)
    key2 = "recent_priority_list_count"
    value2 = d.get(key2, 0)
    if value1 < value2:
        util.validate_recent_log(key1, value1, key2, value2)

    key1 = "recent_priority_relay_count"
    value1 = d.get(key1, 0)
    key2 = "recent_priority_list_count"
    value2 = d.get(key2, 0)
    if value1 < value2:
        util.validate_recent_log(key1, value1, key2, value2)

    key1 = "recent_priority_relay_count"
    value1 = d.get(key1, 0)
    key2 = "recent_measurement_attempt_count"
    value2 = d.get(key2, 0)
    if value1 < value2:
        util.validate_recent_log(key1, value1, key2, value2)

    key1 = "recent_measurement_attempt_count"
    value1 = d.get(key1, 0)
    key2 = "recent_measurements_excluded_error_count"
    value2 = d.get(key2, 0)
    if value1 < value2:
        util.validate_recent_log(key1, value1, key2, value2)

    key1 = "recent_measurements_excluded_error_count"
    value1 = d.get(key1, 0)
    key2 = "recent_measurements_excluded_failure_count"
    value2 = d.get(key2, 0)
    if value1 < value2:
        util.validate_recent_log(key1, value1, key2, value2)


def validate_recent_keyvalues_with_relaybw(d, sum_d):
    key1 = "recent_priority_relay_count"
    value1 = d.get(key1, 0)
    key2 = "relay_recent_priority_list_sum"
    value2 = sum_d.get(key2, 0)
    # Because the old values might not have been removed/counted at exactly
    # the same time, consider some margin.
    value2_min = value2 - 100
    value2_max = value2 + 100
    if not (value2_min <= value1 <= value2_max):
        logger.warning(
            "%s (%s) and %s (%s) mismatch.", key1, value1, key2, value2
        )
    # Not comparing attempt, since it's the number of different relays
    # attempts, not the total
    key1 = "recent_measurement_failure_count"
    value1 = d.get(key1, 0)
    key2 = "relay_recent_measurement_failure_sum"
    value2 = sum_d.get(key2, 0)
    value2_min = value2 - 10
    value2_max = value2 + 10
    if not (value2_min <= value1 <= value2_max):
        logger.warning(
            "%s (%s) and %s (%s) mismatch.", key1, value1, key2, value2
        )
    # Not comparing error, since it's the number of different relays attempts,
    # not the total
