# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause
"""
Generated using factory_generator 1.0.4.
"""

from .base.relaybw import RelayBwFactoryBase


class RelayBwFactory(RelayBwFactoryBase):
    """
    Factory class for RelayBw
    """
