# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause
"""
Generated using factory_generator 1.0.4.
"""

import factory

from onbasca.onbasca.models.webserver import WebServer


class WebServerFactoryBase(factory.django.DjangoModelFactory):
    url = factory.Faker(provider="url")
    enabled = factory.Faker(provider="random_element", elements=(True, False))
    verify = factory.Faker(provider="pystr", max_chars=255)

    class Meta:
        model = WebServer
        django_get_or_create = ("url",)
