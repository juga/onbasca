# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause
"""
Generated using factory_generator 1.0.4.
"""

import factory
from django.utils import timezone

from onbasca.onbasca.models.consensus import Consensus


class ConsensusFactoryBase(factory.django.DjangoModelFactory):
    valid_after = factory.Faker(
        provider="date_time_between",
        start_date="-27d",
        end_date="now",
        tzinfo=timezone.get_current_timezone(),
    )
    _exits_min_bandwidth = factory.Faker(
        provider="random_int", min=0, max=2147483647
    )
    _exits_min_position = factory.Faker(
        provider="random_int", min=0, max=32767
    )
    _non_exits_min_bandwidth = factory.Faker(
        provider="random_int", min=0, max=2147483647
    )
    _non_exits_min_position = factory.Faker(
        provider="random_int", min=0, max=32767
    )
    _cc_alg_2 = factory.Faker(
        provider="random_element", elements=(True, False)
    )
    _bwscanner_cc_gte_1 = factory.Faker(
        provider="random_element", elements=(True, False)
    )
    _bridge_ratio = factory.Faker(
        provider="pyfloat", left_digits=None, right_digits=None, positive=True
    )

    class Meta:
        model = Consensus
        django_get_or_create = ("valid_after",)
