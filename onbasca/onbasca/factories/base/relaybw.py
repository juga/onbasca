# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause
"""
Generated using factory_generator 1.0.4.
"""

import factory
from django.utils import timezone

from onbasca.onbasca.models.relaybw import RelayBw


class RelayBwFactoryBase(factory.django.DjangoModelFactory):
    fingerprint = factory.Faker(provider="pystr", max_chars=40)
    measured_at = factory.Faker(
        provider="date_time_between",
        start_date="-27d",
        end_date="now",
        tzinfo=timezone.get_current_timezone(),
    )
    updated_at = factory.Faker(
        provider="date_time_between",
        start_date="-27d",
        end_date="now",
        tzinfo=timezone.get_current_timezone(),
    )
    nickname = factory.Faker(provider="pystr", max_chars=19)
    bw = factory.Faker(provider="random_int", min=0, max=2147483647)
    consensus_bandwidth = factory.Faker(
        provider="random_int", min=0, max=2147483647
    )
    consensus_bandwidth_is_unmeasured = factory.Faker(
        provider="random_element", elements=(True, False)
    )
    desc_bw_avg = factory.Faker(provider="random_int", min=0, max=2147483647)
    desc_bw_bur = factory.Faker(provider="random_int", min=0, max=2147483647)
    desc_bw_obs_last = factory.Faker(
        provider="random_int", min=0, max=2147483647
    )
    error_circ = factory.Faker(provider="random_int", min=0, max=32767)
    error_stream = factory.Faker(provider="random_int", min=0, max=32767)
    success = factory.Faker(provider="random_int", min=0, max=32767)
    vote = factory.Faker(provider="random_element", elements=(True, False))
    under_min_report = factory.Faker(
        provider="random_element", elements=(True, False)
    )
    bwfile = factory.SubFactory(
        factory="onbasca.onbasca.factories.BwFileFactory"
    )
    relay = factory.SubFactory(
        factory="onbasca.onbasca.factories.RelayFactory"
    )
    bw_mean = factory.Faker(provider="random_int", min=0, max=2147483647)
    bw_median = factory.Faker(provider="random_int", min=0, max=2147483647)
    desc_bw_obs_mean = factory.Faker(
        provider="random_int", min=0, max=2147483647
    )
    error_destination = factory.Faker(provider="random_int", min=0, max=32767)
    error_misc = factory.Faker(provider="random_int", min=0, max=32767)
    error_second_relay = factory.Faker(provider="random_int", min=0, max=32767)
    master_key_ed25519 = factory.Faker(provider="pystr", max_chars=64)
    time = factory.Faker(
        provider="date_time_between",
        start_date="-27d",
        end_date="now",
        tzinfo=timezone.get_current_timezone(),
    )
    unmeasured = factory.Faker(
        provider="random_element", elements=(True, False)
    )
    scanner = factory.Faker(provider="pystr", max_chars=255)
    relay_in_recent_consensus_count = factory.Faker(
        provider="random_int", min=0, max=32767
    )
    relay_recent_priority_list_count = factory.Faker(
        provider="random_int", min=0, max=32767
    )
    relay_recent_measurement_attempt_count = factory.Faker(
        provider="random_int", min=0, max=32767
    )
    relay_recent_measurement_failure_count = factory.Faker(
        provider="random_int", min=0, max=32767
    )
    relay_recent_measurements_excluded_error_count = factory.Faker(
        provider="random_int", min=0, max=32767
    )
    _bw_filt = factory.Faker(provider="random_int", min=0, max=2147483647)
    _ratio_stream = factory.Faker(
        provider="pyfloat", left_digits=None, right_digits=None, positive=None
    )
    _ratio_filt = factory.Faker(
        provider="pyfloat", left_digits=None, right_digits=None, positive=None
    )
    _ratio = factory.Faker(
        provider="pyfloat", left_digits=None, right_digits=None, positive=None
    )
    _bw_scaled = factory.Faker(
        provider="pyfloat", left_digits=None, right_digits=None, positive=None
    )
    _bw_scaled_limited = factory.Faker(
        provider="pyfloat", left_digits=None, right_digits=None, positive=None
    )
    _bw_scaled_limited_rounded = factory.Faker(
        provider="random_int", min=0, max=2147483647
    )

    class Meta:
        model = RelayBw
