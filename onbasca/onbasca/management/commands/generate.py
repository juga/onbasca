# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

"""Generate a Bandwidth File from the scanner measurements."""
import logging

from onbasca.onbasca import config, util
from onbasca.onbasca.management.commands.common_cmd import OnbascaCommand
from onbasca.onbasca.models.bwfile import BwFile

logger = logging.getLogger(__name__)


class Command(OnbascaCommand):
    help = __doc__

    def add_arguments(self, parser):
        super().add_arguments(config, parser)

    def handle(self, *args, **options):
        super().handle(config, *args, **options)
        util.modify_logging(
            config,
            scan=False,
            generate=True,
            log_level=options.get("log_level", None),
        )
        bwfile = BwFile.objects.generate()
        bwfile.write()
