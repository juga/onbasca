# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

import logging

from django.core.management.base import BaseCommand

from onbasca import __version__
from onbasca.onbasca.management.commands import common

logger = logging.getLogger(__name__)


class OnbascaCommand(BaseCommand):
    def get_version(self):
        return __version__

    def add_arguments(self, config, parser):
        common.add_arguments(config, parser)
        return parser

    def handle(self, config, *args, **options):
        common.handle_cmd(config, *args, **options)
