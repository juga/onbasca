# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

from django.contrib import admin

from onbasca.onbasca.models.relay import Relay


@admin.register(Relay)
class RelayAdmin(admin.ModelAdmin):
    list_display = (
        "fingerprint",
        "relaybws_count",
        "consensuses_count",
        "routerstatuses_count",
        "relaydescs_count",
        "measurements_count",
        "is_exit",
        "_relaydescs_min_bandwidth_mean",
        "_bw_mean",
        "_bw_filt",
        "_ratio_stream",
        "_ratio_filt",
        "_ratio",
        "_bw_scaled",
        "_obj_created_at",
        "_obj_updated_at",
    )
    search_fields = ["fingerprint"]
    readonly_fields = [
        "relaybws",
        "routerstatuses",
        "relaydescs",
        # "measurements",
    ]

    list_filter = ["consensuses"]

    def measurements_count(self, obj):
        return obj.measurements_count()

    measurements_count.admin_order_field = "measurements__count"
