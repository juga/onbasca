# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

from django.contrib import admin

from onbasca.onbasca.models.heartbeat import Heartbeat


@admin.register(Heartbeat)
class HeartbeatAdmin(admin.ModelAdmin):
    list_display = (
        "_obj_created_at",
        "loops_count",
        "measured_count",
        "scanner",
        "measured_percent",
        "previous_measured_percent",
        "_min_percent_relays_to_report_reached_at",
    )
