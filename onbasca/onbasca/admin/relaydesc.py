# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

from django.contrib import admin

from onbasca.onbasca.models.relaydesc import RelayDesc

from .links import relay_change_link


@admin.register(RelayDesc)
class RelayDescAdmin(admin.ModelAdmin):
    list_display = (
        # "master_key_ed25519",
        "fingerprint",
        "nickname",
        # "created_at",
        "published",
        "relay_link",
        "observed_bandwidth",
        "average_bandwidth",
        "burst_bandwidth",
        "_min_bandwidth",
        "can_exit_443",
        "_can_exit_443_strict",
        "_can_exit_v6_443",
        "_hibernating",
        "overload_general",
        "overload_ratelimits",
        "overload_fd_exhausted",
        "_flowctrl_2",
        "_uptime",
    )
    ordering = ("-published",)
    list_filter = [
        "published",
        "can_exit_443",
        "overload_general",
        "overload_ratelimits",
        "overload_fd_exhausted",
    ]
    search_fields = ["nickname", "fingerprint"]

    def relay_link(self, obj):
        if obj.relay:
            return relay_change_link(obj.relay)
        return None

    relay_link.short_description = "Relay"
