# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

from django.contrib import admin

from onbasca.onbasca.models.routerstatus import RouterStatus

from .links import relay_change_link


@admin.register(RouterStatus)
class RouterStatusAdmin(admin.ModelAdmin):
    list_display = (
        # "master_key_ed25519",
        "fingerprint",
        "nickname",
        # "created_at",
        "published",
        "relay_link",
        "consensus",
        "bandwidth",
        "is_unmeasured",
        "is_exit",
        "_obj_created_at",
        "_obj_updated_at",
        "_fast",
        "_stable",
    )
    ordering = ("-published",)
    list_filter = ["is_unmeasured", "consensus", "is_exit", "_stable", "_fast"]
    search_fields = ["nickname", "fingerprint"]

    def relay_link(self, obj):
        if obj.relay:
            return relay_change_link(obj.relay)
        return None

    relay_link.short_description = "Relay"
