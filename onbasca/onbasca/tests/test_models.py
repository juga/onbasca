# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause
import pytest
from django.db import IntegrityError
from django.forms.models import model_to_dict
from django.test import TestCase

from onbasca.onbasca.factories import (
    BwFileFactory,
    ConsensusFactory,
    HeartbeatFactory,
    MeasurementFactory,
    RelayBwFactory,
    RelayDescFactory,
    RelayFactory,
    RouterStatusFactory,
    ScannerFactory,
    WebServerFactory,
)
from onbasca.onbasca.models import (
    BwFile,
    Consensus,
    Heartbeat,
    Measurement,
    Relay,
    RelayBw,
    RelayDesc,
    RouterStatus,
    Scanner,
    WebServer,
)


@pytest.mark.django_db
class TestCaseRelay(TestCase):
    def test_create(self):
        """
        Test the creation of a Relay model using a factory
        """
        _ = RelayFactory.create()
        self.assertEqual(Relay.objects.count(), 1)

    def test_create_batch(self):
        """
        Test the creation of 5 Relay models using a factory
        """
        relays = RelayFactory.create_batch(5)
        self.assertEqual(Relay.objects.count(), 5)
        self.assertEqual(len(relays), 5)

    def test_attribute_count(self):
        """
        Test that all attributes of Relay server are counted. It will count the
        primary key and all editable attributes. This test should break if a
        new attribute is added.
        """
        relay = RelayFactory.create()
        relay_dict = model_to_dict(relay)
        self.assertEqual(len(relay_dict.keys()), 13)

    def test_attribute_content(self):
        """
        Test that all attributes of Relay server have content. This test will
        break if an attributes name is changed.
        """
        relay = RelayFactory.create()
        self.assertIsNotNone(relay._obj_created_at)
        self.assertIsNotNone(relay._obj_updated_at)
        self.assertIsNotNone(relay.fingerprint)
        self.assertIsNotNone(relay._routerstatuses_bandwidth_mean)
        self.assertIsNotNone(relay._relaydescs_min_bandwidth_mean)
        self.assertIsNotNone(relay._dead)
        self.assertIsNotNone(relay._bw_mean)
        self.assertIsNotNone(relay._bw_median)
        self.assertIsNotNone(relay._bw_filt)
        self.assertIsNotNone(relay._ratio_stream)
        self.assertIsNotNone(relay._ratio_filt)
        self.assertIsNotNone(relay._ratio)
        self.assertIsNotNone(relay._bw_scaled)
        # Because the factory doesn't create it to avoid raising max recursion.
        self.assertIsNone(relay._measurement_latest)

    def test__measurement_latest_is_unique(self):
        """
        Tests attribute _measurement_latest of model Relay to see if the
        unique constraint works.
        This test should break if the unique attribute is changed.
        """
        relay = RelayFactory.create()
        # Create the measurement cause the factory is not creating it to
        # avoid max recursion exception.
        measurement = MeasurementFactory.create()
        relay._measurement_latest = measurement
        relay.save()
        relay_02 = RelayFactory.create()
        relay_02._measurement_latest = relay._measurement_latest
        try:
            relay_02.save()
            self.fail("Test should have raised and integrity error")
        except IntegrityError as e:
            self.assertEqual(
                str(e)[:46], "duplicate key value violates unique constraint"
            )


@pytest.mark.django_db
class TestCaseRouterStatus(TestCase):
    def test_create(self):
        """
        Test the creation of a RouterStatus model using a factory
        """
        _ = RouterStatusFactory.create()
        self.assertEqual(RouterStatus.objects.count(), 1)

    def test_create_batch(self):
        """
        Test the creation of 5 RouterStatus models using a factory
        """
        router_statuss = RouterStatusFactory.create_batch(5)
        self.assertEqual(RouterStatus.objects.count(), 5)
        self.assertEqual(len(router_statuss), 5)

    def test_attribute_count(self):
        """
        Test that all attributes of RouterStatus server are counted. It will
        count the primary key and all editable attributes. This test should
        break if a new attribute is added.
        """
        router_status = RouterStatusFactory.create()
        router_status_dict = model_to_dict(router_status)
        self.assertEqual(len(router_status_dict.keys()), 20)

    def test_attribute_content(self):
        """
        Test that all attributes of RouterStatus server have content. This test
        will break if an attributes name is changed.
        """
        router_status = RouterStatusFactory.create()
        self.assertIsNotNone(router_status.id)
        self.assertIsNotNone(router_status._obj_created_at)
        self.assertIsNotNone(router_status._obj_updated_at)
        self.assertIsNotNone(router_status.fingerprint)
        self.assertIsNotNone(router_status.published)
        self.assertIsNotNone(router_status.nickname)
        self.assertIsNotNone(router_status.address)
        self.assertIsNotNone(router_status.bandwidth)
        self.assertIsNotNone(router_status.is_unmeasured)
        self.assertIsNotNone(router_status.is_exit)
        self.assertIsNotNone(router_status.relay)
        self.assertIsNotNone(router_status.consensus)
        self.assertIsNotNone(router_status._exit)
        self.assertIsNotNone(router_status._guard)
        self.assertIsNotNone(router_status._guard_exit)
        self.assertIsNotNone(router_status._middle)
        self.assertIsNotNone(router_status._authority)
        self.assertIsNotNone(router_status._running)
        self.assertIsNotNone(router_status._valid)
        self.assertIsNotNone(router_status._measurement_latest)
        self.assertIsNotNone(router_status._fast)
        self.assertIsNotNone(router_status._stable)


@pytest.mark.django_db
class TestCaseConsensus(TestCase):
    def test_create(self):
        """
        Test the creation of a Consensus model using a factory
        """
        _ = ConsensusFactory.create()
        self.assertEqual(Consensus.objects.count(), 1)

    def test_create_batch(self):
        """
        Test the creation of 5 Consensus models using a factory
        """
        consensuss = ConsensusFactory.create_batch(5)
        self.assertEqual(Consensus.objects.count(), 5)
        self.assertEqual(len(consensuss), 5)

    def test_attribute_count(self):
        """
        Test that all attributes of Consensus server are counted. It will count
        the primary key and all editable attributes. This test should break if
        a new attribute is added.
        """
        consensus = ConsensusFactory.create()
        consensus_dict = model_to_dict(consensus)
        self.assertEqual(len(consensus_dict.keys()), 8)

    def test_attribute_content(self):
        """
        Test that all attributes of Consensus server have content. This test
        will break if an attributes name is changed.
        """
        consensus = ConsensusFactory.create()
        self.assertIsNotNone(consensus._obj_created_at)
        self.assertIsNotNone(consensus._obj_updated_at)
        self.assertIsNotNone(consensus.valid_after)
        self.assertIsNotNone(consensus._exits_min_bandwidth)
        self.assertIsNotNone(consensus._exits_min_position)
        self.assertIsNotNone(consensus._non_exits_min_bandwidth)
        self.assertIsNotNone(consensus._non_exits_min_position)
        self.assertIsNotNone(consensus._cc_alg_2)
        self.assertIsNotNone(consensus._bwscanner_cc_gte_1)
        self.assertIsNotNone(consensus._bridge_ratio)


@pytest.mark.django_db
class TestCaseHeartbeat(TestCase):
    def test_create(self):
        """
        Test the creation of a Heartbeat model using a factory
        """
        _ = HeartbeatFactory.create()
        self.assertEqual(Heartbeat.objects.count(), 1)

    def test_create_batch(self):
        """
        Test the creation of 5 Heartbeat models using a factory
        """
        heartbeats = HeartbeatFactory.create_batch(5)
        self.assertEqual(Heartbeat.objects.count(), 5)
        self.assertEqual(len(heartbeats), 5)

    def test_attribute_count(self):
        """
        Test that all attributes of Heartbeat server are counted. It will count
        the primary key and all editable attributes. This test should break if
        a new attribute is added.
        """
        heartbeat = HeartbeatFactory.create()
        heartbeat_dict = model_to_dict(heartbeat)
        self.assertEqual(len(heartbeat_dict.keys()), 7)

    def test_attribute_content(self):
        """
        Test that all attributes of Heartbeat server have content. This test
        will break if an attributes name is changed.
        """
        heartbeat = HeartbeatFactory.create()
        self.assertIsNotNone(heartbeat.id)
        self.assertIsNotNone(heartbeat._obj_created_at)
        self.assertIsNotNone(heartbeat._obj_updated_at)
        self.assertIsNotNone(heartbeat.loops_count)
        self.assertIsNotNone(heartbeat.measured_count)
        self.assertIsNotNone(heartbeat.measured_percent)
        self.assertIsNotNone(heartbeat.previous_measured_percent)
        self.assertIsNotNone(heartbeat.elapsed_time)
        self.assertIsNotNone(
            heartbeat._min_percent_relays_to_report_reached_at
        )


@pytest.mark.django_db
class TestCaseWebServer(TestCase):
    def test_create(self):
        """
        Test the creation of a WebServer model using a factory
        """
        _ = WebServerFactory.create()
        self.assertEqual(WebServer.objects.count(), 1)

    def test_create_batch(self):
        """
        Test the creation of 5 WebServer models using a factory
        """
        web_servers = WebServerFactory.create_batch(5)
        self.assertEqual(WebServer.objects.count(), 5)
        self.assertEqual(len(web_servers), 5)

    def test_attribute_count(self):
        """
        Test that all attributes of WebServer server are counted. It will count
        the primary key and all editable attributes. This test should break if
        a new attribute is added.
        """
        web_server = WebServerFactory.create()
        web_server_dict = model_to_dict(web_server)
        self.assertEqual(len(web_server_dict.keys()), 4)

    def test_attribute_content(self):
        """
        Test that all attributes of WebServer server have content. This test
        will break if an attributes name is changed.
        """
        web_server = WebServerFactory.create()
        self.assertIsNotNone(web_server.id)
        self.assertIsNotNone(web_server._obj_created_at)
        self.assertIsNotNone(web_server._obj_updated_at)
        self.assertIsNotNone(web_server.url)
        self.assertIsNotNone(web_server.enabled)
        self.assertIsNotNone(web_server.verify)

    def test_url_is_unique(self):
        """
        Tests attribute url of model WebServer to see if the unique constraint
        works. This test should break if the unique attribute is changed.
        """
        web_server = WebServerFactory.create()
        web_server_02 = WebServerFactory.create()
        web_server_02.url = web_server.url
        try:
            web_server_02.save()
            self.fail("Test should have raised and integrity error")
        except IntegrityError as e:
            self.assertEqual(
                str(e)[:46], "duplicate key value violates unique constraint"
            )


@pytest.mark.django_db
class TestCaseMeasurement(TestCase):
    def test_create(self):
        """
        Test the creation of a Measurement model using a factory
        """
        _ = MeasurementFactory.create()
        self.assertEqual(Measurement.objects.count(), 1)

    def test_create_batch(self):
        """
        Test the creation of 5 Measurement models using a factory
        """
        measurements = MeasurementFactory.create_batch(5)
        self.assertEqual(Measurement.objects.count(), 5)
        self.assertEqual(len(measurements), 5)

    def test_attribute_count(self):
        """
        Test that all attributes of Measurement server are counted. It will
        count the primary key and  all editable attributes. This test should
        break if a new attribute is added.
        """
        measurement = MeasurementFactory.create()
        measurement_dict = model_to_dict(measurement)
        self.assertEqual(len(measurement_dict.keys()), 11)

    def test_attribute_content(self):
        """
        Test that all attributes of Measurement server have content. This test
        will break if an attributes name is changed.
        """
        measurement = MeasurementFactory.create()
        self.assertIsNotNone(measurement.id)
        self.assertIsNotNone(measurement._obj_created_at)
        self.assertIsNotNone(measurement._obj_updated_at)
        self.assertIsNotNone(measurement.consensus)
        self.assertIsNotNone(measurement.created_at)
        self.assertIsNotNone(measurement.queued_at)
        self.assertIsNotNone(measurement.attempted_at)
        self.assertIsNotNone(measurement.finished_at)
        self.assertIsNotNone(measurement.relay)
        self.assertIsNotNone(measurement.webserver)
        self.assertIsNotNone(measurement.helper)
        self.assertIsNotNone(measurement.as_exit)
        self.assertIsNotNone(measurement.bandwidth)
        self.assertIsNotNone(measurement.error)


@pytest.mark.django_db
class TestCaseRelayBw(TestCase):
    def test_create(self):
        """
        Test the creation of a RelayBw model using a factory
        """
        _ = RelayBwFactory.create()
        self.assertEqual(RelayBw.objects.count(), 1)

    def test_create_batch(self):
        """
        Test the creation of 5 RelayBw models using a factory
        """
        relay_bws = RelayBwFactory.create_batch(5)
        self.assertEqual(RelayBw.objects.count(), 5)
        self.assertEqual(len(relay_bws), 5)

    def test_attribute_count(self):
        """
        Test that all attributes of RelayBw server are counted. It will count
        the primary key and all editable attributes. This test should break if
        a new attribute is added.
        """
        relay_bw = RelayBwFactory.create()
        relay_bw_dict = model_to_dict(relay_bw)
        self.assertEqual(len(relay_bw_dict.keys()), 40)

    def test_attribute_content(self):
        """
        Test that all attributes of RelayBw server have content. This test will
        break if an attributes name is changed.
        """
        relay_bw = RelayBwFactory.create()
        self.assertIsNotNone(relay_bw.id)
        self.assertIsNotNone(relay_bw._obj_created_at)
        self.assertIsNotNone(relay_bw._obj_updated_at)
        self.assertIsNotNone(relay_bw.fingerprint)
        self.assertIsNotNone(relay_bw.measured_at)
        self.assertIsNotNone(relay_bw.updated_at)
        self.assertIsNotNone(relay_bw.nickname)
        self.assertIsNotNone(relay_bw.bw)
        self.assertIsNotNone(relay_bw.consensus_bandwidth)
        self.assertIsNotNone(relay_bw.consensus_bandwidth_is_unmeasured)
        self.assertIsNotNone(relay_bw.desc_bw_avg)
        self.assertIsNotNone(relay_bw.desc_bw_bur)
        self.assertIsNotNone(relay_bw.desc_bw_obs_last)
        self.assertIsNotNone(relay_bw.error_circ)
        self.assertIsNotNone(relay_bw.error_stream)
        self.assertIsNotNone(relay_bw.relay_in_recent_consensus_count)
        self.assertIsNotNone(relay_bw.relay_recent_measurement_attempt_count)
        self.assertIsNotNone(relay_bw.relay_recent_measurement_failure_count)
        self.assertIsNotNone(
            relay_bw.relay_recent_measurements_excluded_error_count
        )
        self.assertIsNotNone(relay_bw.relay_recent_priority_list_count)
        self.assertIsNotNone(relay_bw.success)
        self.assertIsNotNone(relay_bw.vote)
        self.assertIsNotNone(relay_bw.under_min_report)
        self.assertIsNotNone(relay_bw.bwfile)
        self.assertIsNotNone(relay_bw.relay)
        self.assertIsNotNone(relay_bw.bw_mean)
        self.assertIsNotNone(relay_bw.bw_median)
        self.assertIsNotNone(relay_bw.desc_bw_obs_mean)
        self.assertIsNotNone(relay_bw.error_destination)
        self.assertIsNotNone(relay_bw.error_misc)
        self.assertIsNotNone(relay_bw.error_second_relay)
        self.assertIsNotNone(relay_bw.master_key_ed25519)
        self.assertIsNotNone(relay_bw.time)
        self.assertIsNotNone(relay_bw.unmeasured)
        self.assertIsNotNone(relay_bw.scanner)
        self.assertIsNotNone(relay_bw._bw_filt)
        self.assertIsNotNone(relay_bw._ratio_stream)
        self.assertIsNotNone(relay_bw._ratio_filt)
        self.assertIsNotNone(relay_bw._ratio)
        self.assertIsNotNone(relay_bw._bw_scaled)
        self.assertIsNotNone(relay_bw._bw_scaled_limited)
        self.assertIsNotNone(relay_bw._bw_scaled_limited_rounded)


@pytest.mark.django_db
class TestCaseRelayDesc(TestCase):
    def test_create(self):
        """
        Test the creation of a RelayDesc model using a factory
        """
        _ = RelayDescFactory.create()
        self.assertEqual(RelayDesc.objects.count(), 1)

    def test_create_batch(self):
        """
        Test the creation of 5 RelayDesc models using a factory
        """
        relay_descs = RelayDescFactory.create_batch(5)
        self.assertEqual(RelayDesc.objects.count(), 5)
        self.assertEqual(len(relay_descs), 5)

    def test_attribute_count(self):
        """
        Test that all attributes of RelayDesc server are counted. It will count
        the primary key and all editable attributes. This test should break if
        a new attribute is added.
        """
        relay_desc = RelayDescFactory.create()
        relay_desc_dict = model_to_dict(relay_desc)
        self.assertEqual(len(relay_desc_dict.keys()), 19)

    def test_attribute_content(self):
        """
        Test that all attributes of RelayDesc server have content. This test
        will break if an attributes name is changed.
        """
        relay_desc = RelayDescFactory.create()
        self.assertIsNotNone(relay_desc.id)
        self.assertIsNotNone(relay_desc._obj_created_at)
        self.assertIsNotNone(relay_desc._obj_updated_at)
        self.assertIsNotNone(relay_desc.fingerprint)
        self.assertIsNotNone(relay_desc.published)
        self.assertIsNotNone(relay_desc.ed25519_master_key)
        self.assertIsNotNone(relay_desc.nickname)
        self.assertIsNotNone(relay_desc.observed_bandwidth)
        self.assertIsNotNone(relay_desc.average_bandwidth)
        self.assertIsNotNone(relay_desc.burst_bandwidth)
        self.assertIsNotNone(relay_desc._min_bandwidth)
        self.assertIsNotNone(relay_desc.can_exit_443)
        self.assertIsNotNone(relay_desc._can_exit_443_strict)
        self.assertIsNotNone(relay_desc._can_exit_v6_443)
        self.assertIsNotNone(relay_desc.relay)
        self.assertIsNotNone(relay_desc._hibernating)
        self.assertIsNotNone(relay_desc.overload_general)
        self.assertIsNotNone(relay_desc.overload_ratelimits)
        self.assertIsNotNone(relay_desc.overload_fd_exhausted)
        self.assertIsNotNone(relay_desc._flowctrl_2)
        self.assertIsNotNone(relay_desc._uptime)


@pytest.mark.django_db
class TestCaseScanner(TestCase):
    def test_create(self):
        """
        Test the creation of a Scanner model using a factory
        """
        _ = ScannerFactory.create()
        self.assertEqual(Scanner.objects.count(), 1)

    def test_create_batch(self):
        """
        Test the creation of 5 Scanner models using a factory
        """
        try:
            _ = ScannerFactory.create_batch(5)
        except IntegrityError as e:
            self.assertEqual(
                # id, uuid and heartbeat are all unique and this error raises
                # when trying to create another `Scanner` instance.
                str(e)[:46],
                "duplicate key value violates unique constraint",
            )

    def test_attribute_count(self):
        """
        Test that all attributes of Scanner server are counted. It will count
        the primary key and all editable attributes. This test should break if
        a new attribute is added.
        """
        scanner = ScannerFactory.create()
        scanner_dict = model_to_dict(scanner)
        self.assertEqual(len(scanner_dict.keys()), 7)

    def test_attribute_content(self):
        """
        Test that all attributes of Scanner server have content. This test will
        break if an attributes name is changed.
        """
        scanner = ScannerFactory.create()
        self.assertIsNotNone(scanner.id)
        self.assertIsNotNone(scanner._obj_created_at)
        self.assertIsNotNone(scanner._obj_updated_at)
        self.assertIsNotNone(scanner.started_at)
        self.assertIsNotNone(scanner.elapsed_time)
        self.assertIsNotNone(scanner.uuid)
        self.assertIsNotNone(scanner.nickname)
        self.assertIsNotNone(scanner.tor_version)
        self.assertIsNotNone(scanner.heartbeat)


@pytest.mark.django_db
class TestCaseBwFile(TestCase):
    def test_create(self):
        """
        Test the creation of a BwFile model using a factory
        """
        _ = BwFileFactory.create()
        self.assertEqual(BwFile.objects.count(), 1)

    def test_create_batch(self):
        """
        Test the creation of 5 BwFile models using a factory
        """
        bw_files = BwFileFactory.create_batch(5)
        self.assertEqual(BwFile.objects.count(), 5)
        self.assertEqual(len(bw_files), 5)

    def test_attribute_count(self):
        """
        Test that all attributes of BwFile server are counted. It will count
        the primary key and all editable attributes. This test should break if
        a new attribute is added.
        """
        bw_file = BwFileFactory.create()
        bw_file_dict = model_to_dict(bw_file)
        self.assertEqual(len(bw_file_dict.keys()), 35)

    def test_attribute_content(self):
        """
        Test that all attributes of BwFile server have content. This test will
        break if an attributes name is changed.
        """
        bw_file = BwFileFactory.create()
        self.assertIsNotNone(bw_file.id)
        self.assertIsNotNone(bw_file._obj_created_at)
        self.assertIsNotNone(bw_file._obj_updated_at)
        self.assertIsNotNone(bw_file.file_created)
        self.assertIsNotNone(bw_file.latest_bandwidth)
        self.assertIsNotNone(bw_file.scanner_country)
        self.assertIsNotNone(bw_file.software)
        self.assertIsNotNone(bw_file.software_version)
        self.assertIsNotNone(bw_file.tor_version)
        self.assertIsNotNone(bw_file.consensus)
        self.assertIsNotNone(bw_file.destinations_countries)
        self.assertIsNotNone(bw_file.earliest_bandwidth)
        self.assertIsNotNone(bw_file.generator_started)
        self.assertIsNotNone(bw_file.minimum_number_eligible_relays)
        self.assertIsNotNone(bw_file.minimum_percent_eligible_relays)
        self.assertIsNotNone(bw_file.number_consensus_relays)
        self.assertIsNotNone(bw_file.number_eligible_relays)
        self.assertIsNotNone(bw_file.percent_eligible_relays)
        self.assertIsNotNone(bw_file.recent_consensus_count)
        self.assertIsNotNone(bw_file.recent_priority_list_count)
        self.assertIsNotNone(bw_file.recent_priority_relay_count)
        self.assertIsNotNone(bw_file.recent_measurement_attempt_count)
        self.assertIsNotNone(bw_file.recent_measurement_failure_count)
        self.assertIsNotNone(bw_file.recent_measurements_excluded_error_count)
        self.assertIsNotNone(bw_file.recent_measurements_excluded_near_count)
        self.assertIsNotNone(bw_file.recent_measurements_excluded_few_count)
        self.assertIsNotNone(bw_file.recent_measurements_excluded_old_count)
        self.assertIsNotNone(bw_file.time_to_report_half_network)
        self.assertIsNotNone(bw_file.version)
        self.assertIsNotNone(bw_file._mu)
        self.assertIsNotNone(bw_file._muf)
        self.assertIsNotNone(bw_file._bw_sum)
        self.assertIsNotNone(bw_file._bw_scaled_sum)
        self.assertIsNotNone(bw_file._limit)
        self.assertIsNotNone(bw_file._bw_scaled_limited_sum)
        self.assertIsNotNone(bw_file._bw_scaled_limited_rounded_sum)
        self.assertIsNotNone(bw_file._heartbeat)

    def test_file_created_is_unique(self):
        """
        Tests attribute file_created of model BwFile to see if the unique
        constraint works. This test should break if the unique attribute is
        changed.
        """
        bw_file = BwFileFactory.create()
        bw_file_02 = BwFileFactory.create()
        bw_file_02.file_created = bw_file.file_created
        try:
            bw_file_02.save()
            self.fail("Test should have raised and integrity error")
        except IntegrityError as e:
            self.assertEqual(
                str(e)[:46], "duplicate key value violates unique constraint"
            )
