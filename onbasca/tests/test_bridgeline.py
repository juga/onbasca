# SPDX-FileCopyrightText: 2023 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause
from onbasca.bridgeline import parse_bridge_line


def good_bridge_line_test(
    bridgeline,
    test_addrport,
    test_fingerprint,
    test_obfs4_name,
    test_socks_args,
):
    addrport, fingerprint, obfs4_name, socks_args = parse_bridge_line(
        bridgeline
    )
    assert addrport == test_addrport
    assert fingerprint == test_fingerprint
    assert obfs4_name == obfs4_name
    assert socks_args == test_socks_args


def bad_bridge_line_test(bridgeline):
    assert parse_bridge_line(bridgeline) is None


def test_parse_bridgeline():
    good_bridge_line_test("192.0.2.1:4123", "192.0.2.1:4123", None, None, None)
    good_bridge_line_test("192.0.2.1", "192.0.2.1:443", None, None, None)
    good_bridge_line_test("obfs4 [::1]", "[::1]:443", None, "obfs4", None)
    good_bridge_line_test(
        "obfs4 192.0.2.1:12 "
        "4352e58420e68f5e40bf7c74faddccd9d1349413 two-two=five",
        "192.0.2.1:12",
        "4352e58420e68f5e40bf7c74faddccd9d1349413",
        "obfs4",
        "two-two=five",
    )
    good_bridge_line_test(
        "obfs4 192.0.2.1:12 "
        "4352e58420e68f5e40bf7c74faddccd9d1349413 twoandtwo=five z=z",
        "192.0.2.1:12",
        "4352e58420e68f5e40bf7c74faddccd9d1349413",
        "obfs4",
        "twoandtwo=five z=z",
    )
    good_bridge_line_test(
        "192.0.2.1:1231 4352e58420e68f5e40bf7c74faddccd9d1349413",
        "192.0.2.1:1231",
        "4352e58420e68f5e40bf7c74faddccd9d1349413",
        None,
        None,
    )
    # Empty line
    bad_bridge_line_test("")
    # bad obfs4 name
    bad_bridge_line_test("tr$n_sp0r7 190.20.2.2")
    # weird ip address
    bad_bridge_line_test("a.b.c.d")
    # invalid fingerprint
    bad_bridge_line_test("2.2.2.2:1231 4352e58420e68f5e40bf7c74faddccd9d1349")
    # no k=v in the end
    bad_bridge_line_test(
        "obfs2 2.2.2.2:1231 " "4352e58420e68f5e40bf7c74faddccd9d1349413 what"
    )
    # no addrport
    bad_bridge_line_test("asdw")
    # huge k=v value that can't fit in SOCKS fields
    bad_bridge_line_test(
        "obfs2 2.2.2.2:1231 aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
        "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
        "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
        "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
        "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
        "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
        "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
        "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
        "aa=b"
    )
