#!/bin/bash

# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: CC0-1.0

# Instead of exiting immediately when any of this commands fail,
# the scanner, generate and coverage lines could continue and store there was
# an error on that command. It's just simpler with `-e`.
set -ex

tests/integration/start_chutney.sh

sleep 60

python3 tests/integration/async_https_server.py &>/dev/null &
sleep 5
wget --no-check-certificate -O/dev/null https://localhost:28888/

DJANGO_SETTINGS_MODULE=onbascapr.settings.test ./manage.py makemigrations onbasca
DJANGO_SETTINGS_MODULE=onbascapr.settings.test ./manage.py migrate

# Run actually the scanner
DJANGO_SETTINGS_MODULE=onbascapr.settings.test ./manage.py scan -c tests/integration/data/config.toml
DJANGO_SETTINGS_MODULE=onbascapr.settings.test ./manage.py generate -c tests/integration/data/config.toml

# Run integration tests
# python -m coverage run --append --module pytest -svv tests/integration

tests/integration/stop_chutney.sh
