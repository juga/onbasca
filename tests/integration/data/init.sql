-- SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
--
-- SPDX-License-Identifier: CC0-1.0

CREATE USER myuser with password 'mypass';
CREATE DATABASE mydb;
GRANT ALL PRIVILEGES ON DATABASE mydb TO myuser;
GRANT postgres TO myuser;
ALTER USER myuser CREATEDB;
