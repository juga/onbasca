#!/bin/bash

# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: CC0-1.0

set -e

apt update
apt install -y postgresql postgresql-contrib
pg_ctlcluster 15 main start
cat ./tests/integration/data/init.sql | su - postgres -c psql
