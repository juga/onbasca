#!/bin/bash

# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: CC0-1.0

set -x

CURRENT_DIR=`pwd`
CHUTNEY_DIR=${1:-./chutney}

# If chutney dir already exists, this will fail but it doesn't matter.
git clone https://git.torproject.org/chutney.git $CHUTNEY_DIR

cd $CHUTNEY_DIR
# In case it wasn't cloned recently, pull.
# Since this is run only for the tests, it's ok if the tests fail with a newer
# chutney version, so that we can detect it early.
git pull

# Stop chutney network if it is already running
./chutney stop networks/bwscanner
./chutney configure networks/bwscanner
./chutney start networks/bwscanner
./chutney status networks/bwscanner
./chutney wait_for_bootstrap networks/bwscanner

cd $CURRENT_DIR
