#! /usr/bin/env python

# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

"""Delete objects older than <days>."""
import argparse
import logging
import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "onbascapr.settings")
# Initialize the app
import django  # noqa: E402

if not hasattr(django, 'apps'):
    django.setup()

from onbasca.onbasca.management.commands.clean import Command

logger = logging.getLogger(__name__)


def main():
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    cmd = Command()
    cmd.add_arguments(parser)
    args = parser.parse_args()
    cmd.handle(**args.__dict__)


if __name__ == "__main__":
    main()
