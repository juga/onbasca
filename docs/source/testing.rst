.. _testing:

Installing tests dependencies and running tests
===============================================

To run the tests, extra Python dependencies are needed:

.. literalinclude:: ../../setup.cfg
    :lines: 58-72

To install them from ``onbasca`` ::

    git clone https://gitlab.torproject.org/tpo/network-health/onbasca
    cd onbasca
    pip install .
    pip install .[test]

To run the tests::

    tox

You can also just run one of the ``tox`` environments (see ``tox.ini`` at the
root of this repository), eg::

    tox -e doc

To test that the documentation can be built correctly.

You can also run tests running ``pytest``,
eg. to run the test ``test_ratios``::

    onbrisca/tests/test_bridge_measurement.py::test_ratios

