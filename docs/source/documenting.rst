.. _documenting:

Installing and building the documentation
-----------------------------------------

To build the documentation, extra Python dependencies are needed:

.. literalinclude:: ../../setup.cfg
    :lines: 74-78

To install them from ``onbasca``::

    git clone https://gitlab.torproject.org/tpo/network-health/onbasca
    cd onbasca
    pip install .
    pip install .[doc]

To build the documentation as HTML::

    cd docs/ && make html

The generated HTML will be in ``docs/build/``.

You can view it with any browser opening the URL
``file://<your_path_to_repo>/onbasca/docs/build/html/index.html``

To update the class diagram::

    cd docs/ && make classgraph

The generated diagrams will be in ``docs/source/images/``.

To check that the documentation links are updated, run::

    cd docs/ && make linkcheck
