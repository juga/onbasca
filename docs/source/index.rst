.. SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
..
.. SPDX-License-Identifier: CC0-1.0

.. onbasca documentation master file, created by
   sphinx-quickstart on Thu Mar 18 18:36:24 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. raw:: html

  <script type="text/javascript">
  if (String(window.location).indexOf("readthedocs") !== -1) {
    window.alert('The documentation has moved, redirecting...');
    window.location.replace('https://tpo.pages.torproject.net/network-health/onbasca');
  }
  </script>
  <noscript>
    NOTE: documentation has moved from https://onbasca.readthedocs.org to
    https://tpo.pages.torproject.net/network-health/onbasca
  </noscript>


Welcome to onbasca's documentation!
===================================

``OnBaSca ⚖🌏`` was also the name of a
`Prototype Fund <https://prototypefund.de/en/project/3519-2/>`_ and
`nlnet <https://nlnet.nl/project/OnBaSca/>`_
project to create/improve bandwidth scanners and bandwidth issues in the
Tor network. Most of the documentation generated during those projects has
been moved to
`Bandwidth in Tor <https://tpo.pages.torproject.net/network-health/bandwidth_scanners/index.html>`_.
You can still find the old repository at https://github.com/juga0/onbasca.

This documentation is about Tor's new
`relays and bridges bandwidth scanner <https://gitlab.torproject.org/tpo/network-health/onbasca>`_

User main documentation
------------------------

Included in the [root_directory]_ :

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   README
   INSTALL
   DEPLOY
   config.toml

Developer/technical documentation
----------------------------------

Included in the [docs_directory]_ :

.. toctree::
   :maxdepth: 1

   history
   differences_sbws
   diagrams
   modules
   testing
   documenting
   glossary
   references

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
