.. SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
..
.. SPDX-License-Identifier: CC0-1.0

.. _references:

References
============

.. rubric:: References

.. [aiohttp] https://docs.aiohttp.org/
.. [aiohttp-socks] https://github.com/romis2012/aiohttp-socks
.. [Apache] https://httpd.apache.org/
.. [BandwidthFile] https://gitweb.torproject.org/torspec.git/tree/bandwidth-file-spec.txt
.. [blog_post] https://blog.torproject.org/how-bandwidth-scanners-monitor-tor-network
.. [bwscanner] https://github.com/TheTorProject/BwScanner
.. [colorlog] https://github.com/borntyping/python-colorlog
.. [CoreTorReleases] https://gitlab.torproject.org/tpo/core/team/-/wikis/NetworkTeam/CoreTorReleases#current
.. [Chutney] https://gitlab.torproject.org/tpo/core/chutney
.. [concurrent.futures] https://docs.python.org/3/library/concurrent.futures.html
.. [ContentDeliveryNetwork] https://en.wikipedia.org/wiki/Content_delivery_network
.. [Django] https://www.djangoproject.com/
.. [Django3] https://docs.djangoproject.com/en/3.0/
.. [Django_4.1] https://docs.djangoproject.com/en/4.1/topics/db/queries/#asynchronous-queries
.. [docs_directory]  <https://gitlab.torproject.org/tpo/network-health/sbws/-/tree/main/docs
.. [gunicorn] https://gunicorn.org/
.. [libpq-dev] https://pypi.org/project/libpq-dev/
.. [MetricsGlossary] https://metrics.torproject.org/glossary.html
.. [multiprocessing.dummy] https://docs.python.org/3/library/multiprocessing.html#module-multiprocessing.dummy
.. [nginx] https://nginx.org/
.. [Object-relationalMapping] https://en.wikipedia.org/wiki/Object%E2%80%93relational_mapping
.. [online_documentation] https://tpo.pages.torproject.net/network-health/onbasca/
.. [PostgreSQL] https://www.postgresql.org/
.. [Psycopg2] https://pypi.org/project/psycopg2/
.. [Python3] https://www.python.org/download/releases/3.0/
.. [PyPI] https://pypi.org
.. [requests] https://docs.python-requests.org/
.. [root_directory] https://gitlab.torproject.org/tpo/network-health/onbasca/-/tree/main
.. [sbws] https://gitlab.torproject.org/tpo/network-health/sbws/
.. [setup.cfg] https://gitlab.torproject.org/tpo/network-health/onbasca/-/blob/main/setup.cfg
.. [Shadow] https://shadow.github.io/
.. [socks] http://docs.python-requests.org/en/master/user/advanced/#socks
.. [SQLAlchemy] https://www.sqlalchemy.org/
.. [stem] https://stem.torproject.org/
.. [toml] https://pypi.org/project/toml/
.. [tor] https://gitlab.torproject.org/tpo/core/tor
.. [Torflow] https://gitweb.torproject.org/torflow.git/tree/NetworkScanners/BwAuthority/README.spec.txt
.. [txtorcon] https://txtorcon.readthedocs.io/
.. [virtualenv] https://virtualenv.pypa.io/en/latest/installation.html
