#!/bin/bash
# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: CC0-1.0
# systemctl daemon-reload
set -e

export XDG_RUNTIME_DIR="/run/user/$UID"
export DBUS_SESSION_BUS_ADDRESS="unix:path=${XDG_RUNTIME_DIR}/bus"

echo "Starting services"
systemctl --user daemon-reload
systemctl --user enable --now bridgescan.service
systemctl --user enable --now gunicorn.socket
systemctl --user enable --now gunicorn.service
