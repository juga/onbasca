# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause
from os import environ
# If the environment var is not set, it defaults to production.
if environ.get("BUILD_ENVIRONMENT", "production") == "production":
    from .production import *
# When the environment var is set and it is set to something else than
# production
else:
    from .development import *
