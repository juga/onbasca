# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause
"""Django base settings for onbriscapr project."""
import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

INSTALLED_APPS = [
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "onbasca.onbasca.apps.OnBaScaConfig",
    "onbasca.base.apps.BaseConfig",
    "onbrisca.apps.OnbriscaConfig",
]

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

ROOT_URLCONF = "onbriscapr.urls"
WSGI_APPLICATION = "onbriscapr.wsgi.application"

LANGUAGE_CODE = "en-us"
TIME_ZONE = "UTC"
USE_I18N = True
USE_TZ = False

API_TOKEN_NAME = "Token"
# IMPORTANT: change this in `./local.py`.
API_TOKEN_VALUE = "CHANGEME"

# Custom logging config
LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "nocolor": {
            "format": "{asctime} {module}[{process}]: <{levelname}> "
            + "({threadName}) {filename}:{lineno} - {funcName} - {message}",
            "style": "{",
            "datefmt": "%b %d %H:%M:%S",
        },
        "color": {
            "()": "colorlog.ColoredFormatter",
            "log_colors": {
                "DEBUG": "green",
                "INFO": "blue",
                "WARNING": "bold_yellow",
                "ERROR": "bold_red",
                "CRITICAL": "bold_purple",
            },
            "format": "{log_color}{asctime} {module}[{process}]: <{levelname}> "
            + "({threadName}) {filename}:{lineno} - {funcName} - {message}",
            "style": "{",
            "datefmt": "%b %d %H:%M:%S",
        },
    },
    "filters": {
        "require_debug_true": {"()": "django.utils.log.RequireDebugTrue"}
    },
    "handlers": {
        "console": {
            # "filters": ["require_debug_true"],
            "level": "DEBUG",
            "class": "logging.StreamHandler",
            "formatter": "color",
        },
        "file": {
            "level": "DEBUG",
            "class": "logging.handlers.TimedRotatingFileHandler",
            "filename": "debug.log",
            "when": "midnight",
            "utc": True,
            "backupCount": 84,  # 3 times 28 days
            "formatter": "nocolor",
        },
    },
    "loggers": {
        "onbrisca": {
            "level": "DEBUG",
            "handlers": ["console", "file"],
            "propagate": True,
        },
        "onbasca": {
            "level": "DEBUG",
            "handlers": ["console", "file"],
            "propagate": True,
        },
        "stem": {
            "level": "DEBUG",
            "handlers": ["console", "file"],
            "propagate": True,
        },
        "asyncio": {
            "level": "DEBUG",
            "handlers": ["console", "file"],
            "propagate": True,
        },
        # "scan": {
        #     "level": "DEBUG",
        #     "handlers": ["console", "file"],
        #     "propagate": True,
        # },
        # "generate": {
        #     "level": "DEBUG",
        #     "handlers": ["console", "file"],
        #     "propagate": True,
        # },
        # "__name__": {
        #     "level": "DEBUG",
        #     "handlers": ["console", "file"],
        #     "propagate": True,
        # },
        # gunicorn.error, gunicorn.access
    },
}
