# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause
"""Django production settings for onbriscapr project."""
from os import environ

from .base import *  # noqa: 403


DEBUG = False
ALLOWED_HOSTS = ["localhost", "127.0.0.1"]

from django.core.management.utils import get_random_secret_key  # noqa: E402
SECRET_KEY = environ.get("SECRET_KEY", get_random_secret_key())

# # HTTPS settings
# SESSION_COOKIE_SECURE = True
# CSRF_COOKIE_SECURE = True
# SECURE_SSL_REDIRECT = True

# # HSTS settings
# SECURE_HSTS_SECONDS = 31536000 # 1 year
# SECURE_HSTS_PRELOAD = True
# SECURE_HSTS_INCLUDE_SUBDOMAINS = True

try:
    from .local import *
except ImportError:
    print("Missing local.py in settings.")
