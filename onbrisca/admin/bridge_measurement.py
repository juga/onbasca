# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

from django.contrib import admin

from onbasca.onbasca.admin.links import relay_change_link
from onbrisca.models.bridge_measurement import BridgeMeasurement


@admin.register(BridgeMeasurement)
class BridgeMeasurementAdmin(admin.ModelAdmin):
    list_display = [
        "bridge_link",
        "bandwidth",
        "error",
        "webserver",
        "_obj_created_at",
    ]
    search_fields = ["bridge__fingerprint"]

    def bridge_link(self, obj):
        if obj.bridge:
            return relay_change_link(obj.bridge)
        return None

    bridge_link.short_description = "Bridge"
