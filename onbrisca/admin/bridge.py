# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

from django.contrib import admin

from onbrisca.models.bridge import Bridge


@admin.register(Bridge)
class BridgeAdmin(admin.ModelAdmin):
    list_display = (
        "fingerprint",
        "bridgeline",
        "_obj_created_at",
        "_obj_updated_at",
    )
    search_fields = [
        "fingerprint",
    ]
