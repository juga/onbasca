# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause
"""
Generated using factory_generator 1.0.4.
"""

from onbrisca.factories.base.bridgemeasurement import (
    BridgeMeasurementFactoryBase,
)


class BridgeMeasurementFactory(BridgeMeasurementFactoryBase):
    """
    Factory class for BridgeMeasurement
    """
