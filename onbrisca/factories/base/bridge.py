# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause
"""
Generated using factory_generator 1.0.4.
"""

import factory

from onbrisca.models.bridge import Bridge


class BridgeFactoryBase(factory.django.DjangoModelFactory):
    fingerprint = factory.Faker(provider="pystr", max_chars=40)
    bridgeline = factory.Faker(provider="pystr", max_chars=255)
    _bw_mean = factory.Faker(provider="random_int", min=0, max=2147483647)
    _ratio_stream = factory.Faker(
        provider="pyfloat", left_digits=None, right_digits=None, positive=None
    )

    class Meta:
        model = Bridge
        django_get_or_create = ("fingerprint",)
