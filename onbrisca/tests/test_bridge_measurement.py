# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause
import datetime

import pytest
from freezegun import freeze_time

from onbrisca import config
from onbrisca.factories import BridgeFactory, BridgeMeasurementFactory
from onbrisca.models import Bridge, BridgeMeasurement


@pytest.mark.unit_test
@pytest.mark.django_db
def test_ratios():
    # Create 2 bridges and 2 measurements
    bridge_a = BridgeFactory(fingerprint="A" * 40)
    bridge_b = BridgeFactory(fingerprint="B" * 40)
    BridgeMeasurementFactory(bridge=bridge_a, bandwidth=10)
    BridgeMeasurementFactory(bridge=bridge_b, bandwidth=20)
    mu = Bridge.objects.mu()
    muf = Bridge.objects.muf()
    assert 15.0 == mu == muf
    ratio_a = bridge_a.set_ratios(mu, muf)
    # Ratio of A is ~0.7
    assert bridge_a.is_valid(ratio_a, config.BRIDGE_RATIO_THRESHOLD) == (
        False,
        None,
    )
    assert 10 / 15 == bridge_a._ratio
    ratio_b = bridge_b.set_ratios(mu, muf)
    assert bridge_b.is_valid(ratio_b, config.BRIDGE_RATIO_THRESHOLD) == (
        True,
        None,
    )
    # Ratio of B is ~1.7
    assert 20 / 15 == bridge_b._ratio
    # Create a 3rd bridge and measurement
    bridge_c = BridgeFactory(fingerprint="C" * 40)
    BridgeMeasurementFactory(bridge=bridge_c, bandwidth=30)
    mu = Bridge.objects.mu()
    muf = Bridge.objects.muf()
    assert 20 == mu == muf
    # Ratio of A is 0.5
    ratio_a = bridge_a.set_ratios(mu, muf)
    assert bridge_a.is_valid(ratio_a, config.BRIDGE_RATIO_THRESHOLD) == (
        False,
        None,
    )
    assert 0.5 == bridge_a._ratio
    # Ratio of B is 1
    ratio_b = bridge_b.set_ratios(mu, muf)
    assert bridge_b.is_valid(ratio_b, config.BRIDGE_RATIO_THRESHOLD) == (
        True,
        None,
    )
    assert 1 == bridge_b._ratio
    # Ratio of C is 1.5
    ratio_c = bridge_c.set_ratios(mu, muf)
    assert bridge_c.is_valid(ratio_c, config.BRIDGE_RATIO_THRESHOLD) == (
        True,
        None,
    )
    assert 1.5 == bridge_c._ratio


@pytest.mark.unit_test
@pytest.mark.django_db
def test_delete_old():
    old_date = datetime.datetime.utcnow() - datetime.timedelta(
        days=config.BRIDGE_OLDEST_DATA_DAYS + 1
    )
    with freeze_time(old_date):
        bridge_a = BridgeFactory(fingerprint="A" * 40)
        BridgeMeasurementFactory(bridge=bridge_a)
    date = old_date + datetime.timedelta(days=2)
    with freeze_time(date):
        bridge_b = BridgeFactory(fingerprint="B" * 40)
        BridgeMeasurementFactory(bridge=bridge_b)
    assert 2 == Bridge.objects.count()
    assert 2 == BridgeMeasurement.objects.count()
    Bridge.objects.delete_old()
    assert 1 == Bridge.objects.count()
    assert 1 == BridgeMeasurement.objects.count()
