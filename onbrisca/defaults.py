# SPDX-FileCopyrightText: 2023 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause
"""Defaults for onbrisca."""
import os.path  # noqa:E402

from onbasca.onbasca.defaults import (  # noqa:E402, F401, E501
    EXTERNAL_CONTROL_PORT,
    HOME,
)

# paths
APP_DATA_PATH = os.path.join(HOME, ".onbrisca")
TOR_DATA_PATH = os.path.join(APP_DATA_PATH, "tor")
CONFIG_PATH = os.path.join(APP_DATA_PATH, "config.toml")
LOGS_DIR = os.path.join(APP_DATA_PATH, "logs")
SCAN_LOG_PATH = os.path.join(LOGS_DIR, "scan.log")
CLEAN_LOG_PATH = os.path.join(LOGS_DIR, "clean.log")

# For tor

TOR_CONFIG_BASE = {
    "SocksPort": "auto",
    "CookieAuthentication": "1",
    "UseMicrodescriptors": "0",
    # Here there is no need to get relay descriptors as soon as possible.
    # To build the circuits "manually".
    "LearnCircuitBuildTimeout": "0",
    # This option is partly implemented.
    # "UpdateBridgesFromAuthority": "1",
    # obfs4 transport
    "ClientTransportPlugin": "obfs4 exec /usr/bin/obfs4proxy",
}
TOR_CONFIG_DIRS = {
    # "ControlPort": "8015",
    "ControlSocket": os.path.join(TOR_DATA_PATH, "control"),
    "DataDirectory": TOR_DATA_PATH,
    "PidFile": os.path.join(TOR_DATA_PATH, "pid"),
    "Log": [
        "INFO file {}".format(os.path.join(TOR_DATA_PATH, "info.log")),
        "NOTICE file {}".format(os.path.join(TOR_DATA_PATH, "notices.log")),
    ],
}
TOR_CONFIG = {**TOR_CONFIG_BASE, **TOR_CONFIG_DIRS}

BRIDGESCAN = True
# For calculating bridges ratios
BRIDGE_RATIO_THRESHOLD = 0.9
BRIDGE_RATIO_CONSENSUS_PARAM = "bridge_ratio"
# In how many seconds should the scanner run again when there aren't bridges
# to measure in the queue.
BRIDGESCAN_DURATION = 60
API_PATH = "bridge-state/"
# Number of bridges to measure in each loop
NUM_BRIDGES_LOOP = 25

# Seconds to sleep before setting other bridgeline (see #160)
SET_BRIDGE_SLEEP_SECS = 0.5
# Whether or not to try set bridgelines one by one before setting all of them
# (see #160)
TRY_SET_BRIDGELINES = False
REGEX_PT_SOCKS_ARGS = r"[\w-]+=.+"
PT_TRANSPORTS = ["obfs4"]
# For the measurements
BRIDGE_OLDEST_DATA_DAYS = 28
NUM_THREADS = 3
