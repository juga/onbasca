# SPDX-FileCopyrightText: 2023 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

"""Delete objects older than <days>."""
import datetime
import logging

from onbasca.onbasca import util
from onbasca.onbasca.management.commands.common_cmd import OnbascaCommand
from onbrisca import config
from onbrisca.models.bridge_scanner import BridgeScanner

logger = logging.getLogger(__name__)
now = datetime.datetime.utcnow()


class Command(OnbascaCommand):
    help = __doc__

    def add_arguments(self, parser):
        super().add_arguments(config, parser)
        parser.add_argument(
            "-d",
            "--days",
            default=config.BRIDGE_OLDEST_DATA_DAYS,
            type=int,
            help="How many days in the past data is considered old.",
        )

    def handle(self, *args, **options):
        super().handle(config, *args, **options)
        days = options.get("days", None)
        if days is None:  # Accept "0" days
            days = config.BRIDGE_OLDEST_DATA_DAYS
        util.modify_logging(
            config,
            scan=False,
            generate=False,
            log_level=options.get("log_level", None),
        )
        scanner = BridgeScanner.load()
        scanner.delete_old_objects(days)
        logger.info("Deleted objects older than {} days".format(days))
